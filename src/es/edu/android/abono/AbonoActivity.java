package es.edu.android.abono;

import android.app.Activity;
import android.os.Bundle;
import android.widget.*;
import android.view.View.*;
import android.content.Context;

import java.util.*;

public class AbonoActivity extends Activity {
	Context ctx;
	Button btNuevo;
	TextView txtNuevo;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.abono);
		
		ctx = this;
		
		btNuevo = (Button) findViewById(R.id.btAbonoNuevo);
		txtNuevo = (TextView) findViewById(R.id.txtAbonoNuevo);
		
		btNuevo.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(android.view.View v) {
				String val = txtNuevo.getText().toString();
				if (val != null && !val.equals("")) {
					String text = "Grabar " + (new Date()).toString() +
						" para el mes " + val;
					Toast.makeText(ctx, text, Toast.LENGTH_LONG).show();
				} else {
					Toast.makeText(ctx, "Pon el nombre del mes", Toast.LENGTH_LONG).show();
				}
			}
		});
	}
}
